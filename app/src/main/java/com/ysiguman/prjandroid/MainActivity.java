package com.ysiguman.prjandroid;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import de.codecrafters.tableview.TableView;
import de.codecrafters.tableview.toolkit.SimpleTableDataAdapter;
import de.codecrafters.tableview.toolkit.SimpleTableHeaderAdapter;

public class MainActivity extends AppCompatActivity {

    private static final String[] TABLE_HEADERS = { "id", "name", "type", "price"};

    private static final String[][] DATA_TO_SHOW = {
            {
                    "1",
                    "Apple",
                    "Fruit",
                    "1.2"
            },
            {
                    "2",
                    "Pear",
                    "Fruit",
                    "1.3"
            },
            {
                    "3",
                    "Banana",
                    "Fruit",
                    "1.1"
            },
            {
                    "4",
                    "Mushroom",
                    "Vegetable",
                    "2.7"
            },
            {
                    "5",
                    "Pepper",
                    "Vegetable",
                    "3.5"
            }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        String url = "http://gr3.ega.tf/json.php";
        setContentView(R.layout.activity_main);
        new Connection().execute(url);


        TableView<String[]> tableView = (TableView<String[]>) findViewById(R.id.tableView);
        tableView.setDataAdapter(new SimpleTableDataAdapter(this, DATA_TO_SHOW));
        tableView.setHeaderAdapter(new SimpleTableHeaderAdapter(this, TABLE_HEADERS));
    }
}
